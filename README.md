#  iTunesLibrary

A simple iOS application to fetch movies data, using [iTunes Search API](https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/Searching.html#//apple_ref/doc/uid/TP40017632-CH5-SW1)

## Installation
- Run `pod install`
- Open `iTunesLibrary.xcworkspace` and run the app.

## Architecture
VIPER Architecture
![viper-arch](https://miro.medium.com/max/2000/1*6W73TuYu1DWi9JY4_Uh8aA.png)

## 3rd-parties
* [SwiftLint](https://github.com/realm/SwiftLint)
* [RealmSwift](https://docs.mongodb.com/realm-legacy/docs/swift/latest/index.html)

## Enhancement
Due to time-constraint in this assignment project, the following task could be implemented:
* UnitTests & UI Tests automation tests
* CI/CD integration
