//
//  PlaylistPersistenceService.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import Foundation

protocol PlaylistPersistenceServiceProtocol: class {
    var baseService: PersistenceServiceProtocol { get }

    func saveItemList(items: [PlaylistTrack])
    func markFavourite(item: PlaylistTrack, marked: Bool)
    func fetchItemList(keyword: String?, completion: @escaping (_ items: [PlaylistTrack]) -> Void)
    func fetchBookmarkedItems(completion: @escaping (_ items: [PlaylistTrack]) -> Void)
}

final class PlaylistPersistenceService: PlaylistPersistenceServiceProtocol {

    var baseService: PersistenceServiceProtocol

    init(base: PersistenceServiceProtocol) {
        self.baseService = base
    }

    func saveItemList(items: [PlaylistTrack]) {
        baseService.saveRecords(items: items)
    }

    func markFavourite(item: PlaylistTrack, marked: Bool) {
        baseService.saveItem(item, transaction: { realm in
            let updatedItem = item
            updatedItem.isFavourite = marked
            realm.add(item, update: .modified)
        })
    }

    func fetchItemList(keyword: String?, completion: @escaping (_ items: [PlaylistTrack]) -> Void) {
        var query: String?
        if let keyword = keyword, !keyword.isEmpty {
            query = "name BEGINSWITH '\(keyword)'"
        }
        baseService.fetchItems(query: query) { items in
            completion(items)
        }
    }

    func fetchBookmarkedItems(completion: @escaping (_ items: [PlaylistTrack]) -> Void) {
        baseService.fetchItems(query: "isFavourite = true") { items in
            completion(items)
        }
    }
}
