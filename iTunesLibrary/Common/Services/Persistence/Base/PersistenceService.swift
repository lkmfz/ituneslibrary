//
//  PersistenceService.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//  Realm Doc: https://docs.mongodb.com/realm-legacy/docs/swift/latest/#installation

import Foundation
import RealmSwift

protocol PersistenceServiceProtocol: class {
    func fetchItems<T: Object>(query: String?, completion: @escaping (_ items: [T]) -> Void)
    func saveItem<T: Object>(_ item: T, transaction: @escaping ((_ realm: Realm) -> Void))
    func saveRecords<T: Object>(items: [T])
}

final class PersistenceService: PersistenceServiceProtocol {

    let queue = DispatchQueue.main

    func fetchItems<T: Object>(query: String?, completion: @escaping (_ items: [T]) -> Void) {
        queue.async {
            var result: [T]
            do {
                let realm = try Realm()
                if let query = query, !query.isEmpty {
                    result = realm.objects(T.self).filter(query).map({ $0 })
                } else {
                    result = realm.objects(T.self).map({ $0 })
                }
            } catch let error {
                print("📦[PERSISTANCE_SERVICE] ❌ ERROR: \(error)")
                result = []
            }
            completion(result)
        }
    }

    func saveItem<T: Object>(_ item: T, transaction: @escaping ((_ realm: Realm) -> Void)) {
        queue.async {
            do {
                let realm = try Realm()
                try realm.write {
                    transaction(realm)
                }
            } catch let error {
                print("📦[PERSISTANCE_SERVICE] ❌ ERROR: \(error)")
            }
        }
    }

    func saveRecords<T: Object>(items: [T]) {
        queue.async {
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(items, update: .modified)
                }
            } catch let error {
                print("📦[PERSISTANCE_SERVICE] ❌ ERROR: \(error)")
            }
        }
    }
}

extension PersistenceService {
    class func performMigrationIfNeeded() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { _, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if oldSchemaVersion < 1 {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            })

        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config

        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        _ = try? Realm()
    }
}
