//
//  Networking.swift
//  TrafficImages
//
//  Created by Luqman Fauzi on 09/07/21.
//

import Foundation

final class Networking {

    typealias Response = (res: HTTPURLResponse, data: Data)

    /// List of base URL
    enum Host: String {
        case itunes = "itunes.apple.com"
    }

    /// List of URL's path or endpoint
    enum Endpoint: String {
        case search = "/search"
        case lookup = "/lookup"
    }

    /// List of HTTP method
    enum Method: String {
        case get, post, patch, put, delete
    }
}

/// Type of API error
public enum NetworkingError: Error {
    case invalidStatusCode(Int)
    case reachedRequestLimit(TimeInterval)
    case invalidResponse
    case invalidData
    case emptyList
    case generalError

    var errorMessage: String {
        switch self {
        case .invalidStatusCode(let statusCode):
            return "Invalid status code: \(statusCode)"
        case .emptyList:
            return "Result list is empty"
        case .reachedRequestLimit(let resetTime):
            // GitHub's API requests limit
            let date = Date(timeIntervalSince1970: resetTime)
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
            return "API request has reached hourly count limit. \nIt will be available again after\n\(formatter.string(from: date))"
        default:
            return "Sorry, something went wrong.. 😢"
        }
    }
}

extension URL {
    static func construct(_ host: Networking.Host, _ endpoint: Networking.Endpoint, queries: [String: String] = [:]) -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = host.rawValue
        components.path = endpoint.rawValue
        components.queryItems = queries.map({ URLQueryItem(name: $0.key, value: $0.value) })
        return components.url
    }
}
