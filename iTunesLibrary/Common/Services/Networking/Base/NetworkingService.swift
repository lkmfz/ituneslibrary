//
//  NetworkingService.swift
//  TrafficImages
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

protocol NetworkingServiceProtocol: class {
    func request(_ method: Networking.Method, _ url: URL?, headers: [String: String], parameters: [String: Any], completion: @escaping (_ result: Result<Networking.Response, Error>) -> Void)
}

final class NetworkingService: NetworkingServiceProtocol {

    /// Make HTTP request
    /// - Parameter method: HTTP method. i.e: "GET", "POST", "PATCH", etc
    /// - Parameter url: request's url
    /// - Parameter headers: request's header
    /// - Parameter parameters: request's parameters
    /// - Parameter completion: completion event with HTTPResponse & Data or error result
    func request(_ method: Networking.Method, _ url: URL?, headers: [String: String], parameters: [String: Any], completion: @escaping (_ result: Result<Networking.Response, Error>) -> Void) {

        guard let url = url else {
            fatalError("Invalid URL")
        }

        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringCacheData
        request.allHTTPHeaderFields = ["accept": "application/json"].merging(headers) { (_, new) in new }
        request.httpMethod = method.rawValue.uppercased()

        if method != .get {
            // As per iOS13, GET request is not allowed to append params in the body
            // (https://stackoverflow.com/a/56973866)
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        }

        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(NetworkingError.invalidResponse))
                return
            }
            guard 200..<300 ~= httpResponse.statusCode else {
                completion(.failure(NetworkingError.invalidStatusCode(httpResponse.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkingError.invalidData))
                return
            }

            let res = String(data: data, encoding: .utf8) ?? "N/A"
            let url = request.url?.absoluteString ?? ""
            print("▶️[NETWORKING_SERVICE] \(url)\n\(res)")

            let response: Networking.Response = (httpResponse, data)
            completion(.success(response))
        })

        task.resume()
    }
}
