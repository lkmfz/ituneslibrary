//
//  PlaylistNetworkingService.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//  API Doc: https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/Searching.html#//apple_ref/doc/uid/TP40017632-CH5-SW1

import Foundation
import UIKit

protocol PlaylistNetworkingServiceProtocol: class {
    var baseService: NetworkingServiceProtocol { get }

    func searchItems(queries: [String: String], completion: @escaping (_ result: Result<[PlaylistTrack], Error>) -> Void)
    func lookupDetailsItem(id: Int, completion: @escaping (_ result: Result<PlaylistTrack, Error>) -> Void)
    func loadMedia(from url: String?, placeholder: UIImage?, completion: @escaping ((_ image: UIImage?) -> Void))
}

final class PlaylistNetworkingService: PlaylistNetworkingServiceProtocol {

    var baseService: NetworkingServiceProtocol

    init(base: NetworkingServiceProtocol) {
        self.baseService = base
    }

    /// Fetch items based on queries
    /// - Parameters:
    ///   - queries: key-value pair queies
    ///   - completion: result callback
    func searchItems(queries: [String: String], completion: @escaping (_ result: Result<[PlaylistTrack], Error>) -> Void) {

        let url = URL.construct(.itunes, .search, queries: queries)

        baseService.request(.get, url, headers: [:], parameters: [:]) { result in
            switch result {
            case .success(let response):
                do {
                    let json = try JSONDecoder().decode(PlaylistFetchResult.self, from: response.data)
                    let items = json.results

                    guard !items.isEmpty else { throw NetworkingError.emptyList }

                    completion(.success(items))
                } catch let error {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func lookupDetailsItem(id: Int, completion: @escaping (_ result: Result<PlaylistTrack, Error>) -> Void) {
        let queries: [String: String] = ["id": id.description]
        let url = URL.construct(.itunes, .lookup, queries: queries)

        baseService.request(.get, url, headers: [:], parameters: [:]) { result in
            switch result {
            case .success(let response):
                do {
                    let json = try JSONDecoder().decode(PlaylistFetchResult.self, from: response.data)

                    guard let detailsItem = json.results.first else { throw NetworkingError.emptyList }

                    completion(.success(detailsItem))
                } catch let error {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    /// Load image from URL
    /// - Parameters:
    ///   - url: image URL
    ///   - placeholder: placeholder image
    ///   - completion: completion handler
    func loadMedia(from url: String?, placeholder: UIImage?, completion: @escaping ((_ image: UIImage?) -> Void)) {
        guard let url = url, let imageURL = URL(string: url) else {
            completion(placeholder)
            return
        }

        let cache = URLCache.shared
        let request = URLRequest(url: imageURL)
        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, _) in
            if let data = data, let response = response, let image = UIImage(data: data) {
                let cachedData = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedData, for: request)
                DispatchQueue.main.async {
                    completion(image)
                }
            } else {
                completion(placeholder)
            }
        })

        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            completion(image)
        } else {
            dataTask.resume()
        }
    }
}
