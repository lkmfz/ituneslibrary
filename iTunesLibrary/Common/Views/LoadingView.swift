//
//  LoadingView.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

public final class LoadingView: UIView {

    private lazy var indicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .large)
        view.color = .white
        view.startAnimating()
        return view
    }()

    public init() {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        alpha = 0.5
        layer.cornerRadius = 10
        layer.masksToBounds = true
        backgroundColor = .darkGray
        addSubview(indicator)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        indicator.fillToSuperview()
    }

    public func stop() {
        isHidden = true
    }

    public func start() {
        isHidden = false
    }
}
