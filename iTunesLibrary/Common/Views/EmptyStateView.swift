//
//  EmptyStateView.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

final class EmptyStateView: UIView {

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.numberOfLines = 0
        return label
    }()

    public var tapActionHandler: (() -> Void)?

    public init() {
        super.init(frame: .zero)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLayout()
    }

    private func setupLayout() {
        addSubview(messageLabel)
        NSLayoutConstraint.activate([
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            messageLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }

    @objc private func didTapButton() {
        tapActionHandler?()
    }

    public func show(message: String, textColor: UIColor = .red) {
        isHidden = false
        messageLabel.text = message
        messageLabel.textColor = textColor.withAlphaComponent(0.7)
        messageLabel.sizeToFit()
    }

    public func hide() {
        isHidden = true
    }
}
