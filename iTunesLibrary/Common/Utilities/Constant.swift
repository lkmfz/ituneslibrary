//
//  Constant.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import Foundation

struct Constant {
    private init() { }

    struct App {
        static let bundleId = "com.luqmanfauzi.iTunesLibrary"
    }
}
