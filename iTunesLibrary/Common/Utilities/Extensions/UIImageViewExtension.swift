//
//  UIImageViewExtension.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import UIKit

extension UIImageView {

    /// Load image given a URL,
    /// - Parameters:
    ///   - url: image URL
    ///   - placeholder: placeholder image
    func loadImage(from url: String?, placeholder: UIImage?) {
        self.image = nil
        guard let url = url, let imageURL = URL(string: url) else {
            self.image = placeholder
            return
        }

        let cache = URLCache.shared
        let request = URLRequest(url: imageURL)
        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, _) in
            if let data = data, let response = response, let image = UIImage(data: data) {
                let cachedData = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedData, for: request)
                DispatchQueue.main.async {
                    self.image = image
                }
            } else {
                DispatchQueue.main.async {
                    self.image = placeholder
                }
            }
        })

        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            // Load cached image
            self.image = image
        } else {
            dataTask.resume()
        }
    }
}
