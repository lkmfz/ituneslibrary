//
//  StringExtension.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import Foundation

extension String {

    func formatISODate(_ format: String, locale: Locale = Locale.current) -> String? {
        guard let date = ISO8601DateFormatter().date(from: self) else {
            return nil
        }

        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
}
