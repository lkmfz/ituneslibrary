//
//  NotificationExtension.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import Foundation

extension Notification {
    static func custom(identifier: String) -> Notification.Name {
        return Notification.Name(rawValue: Constant.App.bundleId + identifier)
    }
}
