//
//  PlaylistTrack.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import Foundation
import RealmSwift

struct PlaylistFetchResult: Decodable {
    let results: [PlaylistTrack]
}

final class PlaylistTrack: Object, Decodable {

    @objc dynamic var id: Int
    @objc dynamic var name: String
    @objc dynamic var genreName: String
    @objc dynamic var releaseDate: String
    @objc dynamic var price: Float
    @objc dynamic var currency: String
    @objc dynamic var artworkUrl30: String?
    @objc dynamic var artworkUrl100: String?
    @objc dynamic var longDescription: String?
    @objc dynamic var isFavourite: Bool = false

    override static func primaryKey() -> String? {
        return "id"
    }

    enum CodingKeys: String, CodingKey {
        case id = "trackId"
        case name = "trackName"
        case genreName = "primaryGenreName"
        case releaseDate
        case price = "trackPrice"
        case currency
        case artworkUrl30
        case artworkUrl100
        case longDescription
    }
}
