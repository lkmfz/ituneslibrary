//
//  PlaylistInteractor.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

final class PlaylistInteractor: PlaylistPresenterToInteractorProtocol {

    var presenter: PlaylistInteractorToPresenterProtocol
    var apiService: PlaylistNetworkingServiceProtocol
    var persistenceService: PlaylistPersistenceServiceProtocol

    init(presenter: PlaylistInteractorToPresenterProtocol, apiService: PlaylistNetworkingServiceProtocol, persistenceService: PlaylistPersistenceServiceProtocol) {
        self.presenter = presenter
        self.apiService = apiService
        self.persistenceService = persistenceService
    }

    /// Fetch items from API
    /// - Parameter keyword: search's term
    func fetchItemsFromAPI(keyword: String?) {
        let queries: [String: String] = [
            "term": keyword ?? "",
            "media": "movie",
            "entity": "movie",
            "attribute": "movieTerm",
            "limit": "25"
        ]

        apiService.searchItems(queries: queries) { [weak self] result in
            switch result {
            case .success(let list):
                self?.presenter.onReceiveSuccessResultAPI(list)
            case .failure(let error):
                self?.presenter.onFailedResult(error)
            }
        }
    }

    /// Retrieve data from local storage
    /// - Parameters:
    ///   - keyword: search's term keyword
    ///   - needsAPIDataSync: data sync flag  between API & local storage
    ///   - initialSetup: initial setup flag
    func fetchItemsFromLocal(keyword: String?, needsAPIDataSync: Bool, initialSetup: Bool) {
        persistenceService.fetchItemList(keyword: keyword, completion: { [weak self] items in
            self?.presenter.onReceiveResultFromLocal(items, keyword: keyword, needsAPIDataSync: needsAPIDataSync, initialSetup: initialSetup)
        })
    }

    /// Save items to local storage
    /// - Parameter items: items list
    func storeItemsToLocal(_ items: [PlaylistTrack]) {
        persistenceService.saveItemList(items: items)
    }

    /// Mark item as either favourited or un-favourited
    /// - Parameters:
    ///   - item: item object
    ///   - marked: favourited flag
    func setFavouriteMark(item: PlaylistTrack, marked: Bool) {
        persistenceService.markFavourite(item: item, marked: marked)
    }
}
