//
//  PlaylistPresenter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

final class PlaylistPresenter: PlaylistViewToPresenterProtocol {

    var view: PlaylistPresenterToViewProtocol?
    var interactor: PlaylistPresenterToInteractorProtocol?
    var router: PlaylistPresenterToRouterProtocol?

    // MARK: - Business logic
    func searchItems(keyword: String?) {
        // Try to retrieve from local, handler is at `onReceiveSuccessResultAPI`
        interactor?.fetchItemsFromAPI(keyword: keyword)
    }

    func fetchItemsInitialView() {
        // Try to retrieve from local, handler is at `onReceiveSuccessResultLocal`
        interactor?.fetchItemsFromLocal(keyword: nil, needsAPIDataSync: false, initialSetup: true)
    }

    func refreshList(keyword: String?) {
        // Refresh list from the persistend local data
        interactor?.fetchItemsFromLocal(keyword: nil, needsAPIDataSync: false, initialSetup: false)
    }

    func setFavourite(item: PlaylistTrack, marked: Bool) {
        // Set or unset fav mark on item
        interactor?.setFavouriteMark(item: item, marked: marked)
    }

    // MARK: - Navigation
    func redirectToBookmarkPage() {
        guard let view = view else {
            fatalError("View hasn't been set")
        }
        router?.redirectToBookmarkPage(source: view)
    }

    func redirectToDetailsPage(selectedItem: PlaylistTrack) {
        guard let view = view else {
            fatalError("View hasn't been set")
        }
        router?.redirectToDetailsPage(source: view, selectedItem: selectedItem)
    }
}

/// Presenter ← Interactor
extension PlaylistPresenter: PlaylistInteractorToPresenterProtocol {

    /// Retrieve data from API srevice
    /// - Parameter items: retrieved items
    func onReceiveSuccessResultAPI(_ items: [PlaylistTrack]) {
        // Store items to local persitence room
        interactor?.storeItemsToLocal(items)
        // Display to view
        view?.showSearchResultItems(items)
    }

    /// Retrieve data from local database
    /// - Parameters:
    ///   - items: retrieved items
    ///   - keyword: term keyword
    ///   - needsAPIDataSync: data sync flag  between API & local storage
    ///   - initialSetup: initial setup flag
    func onReceiveResultFromLocal(_ items: [PlaylistTrack], keyword: String?, needsAPIDataSync: Bool, initialSetup: Bool) {
        if initialSetup {
            if items.isEmpty {
                // Fetch data from API for a given term key
                interactor?.fetchItemsFromAPI(keyword: keyword)
            } else {
                // Present data to view
                view?.showSearchResultItems(items)
            }
            return
        } else {
            if needsAPIDataSync {
                // Fetch data from API for a given term key
                interactor?.fetchItemsFromAPI(keyword: keyword)
            } else {
                // Present data to view
                view?.showSearchResultItems(items)
            }
        }
    }

    /// If API request is failed or any service error  related
    /// - Parameter error: error message
    func onFailedResult(_ error: Error) {
        // Present error message to view
        view?.showError(errorMessage: error.localizedDescription)
    }
}
