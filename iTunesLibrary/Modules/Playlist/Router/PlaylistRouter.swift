//
//  PlaylistRouter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

final class PlaylistRouter: PlaylistPresenterToRouterProtocol {

    static func createModule() -> PlaylistViewController {
        typealias PlaylistPresenterProtocol = PlaylistViewToPresenterProtocol & PlaylistInteractorToPresenterProtocol

        let presenter: PlaylistPresenterProtocol = PlaylistPresenter()
        let router: PlaylistPresenterToRouterProtocol = PlaylistRouter()
        let view = PlaylistViewController(presenter: presenter)

        // Service dependecies
        let apiService = PlaylistNetworkingService(base: NetworkingService())
        let persistanceService = PlaylistPersistenceService(base: PersistenceService())

        let interactor = PlaylistInteractor(
            presenter: presenter,
            apiService: apiService,
            persistenceService: persistanceService
        )

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor

        return view
    }

    /// Present  bookmarks view page
    /// - Parameter source: movie list page
    func redirectToBookmarkPage(source: PlaylistPresenterToViewProtocol) {
        if let view = source as? PlaylistViewController {
            let navigation = UINavigationController(rootViewController: BookmarkRouter.createModule())
            navigation.modalPresentationStyle = .overFullScreen
            view.present(navigation, animated: true, completion: nil)
        }
    }

    /// Push to details view page
    /// - Parameters:
    ///   - source: movie list page
    ///   - selectedItem: selected item on the list
    func redirectToDetailsPage(source: PlaylistPresenterToViewProtocol, selectedItem: PlaylistTrack) {
        if let view = source as? PlaylistViewController {
            let detailsView = DetailsRouter.createModule(trackItem: selectedItem)
            view.navigationController?.pushViewController(detailsView, animated: true)
        }
    }
}
