//
//  PlaylistTrackCell.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

final class PlaylistTrackCell: UITableViewCell {

    private lazy var artworkImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        return view
    }()

    private lazy var contentStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillProportionally
        view.spacing = 8
        view.isUserInteractionEnabled = true
        return view
    }()

    private lazy var textStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.spacing = 3
        view.isUserInteractionEnabled = true
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .bold)
        return label
    }()

    private lazy var releaseYearLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 15, weight: .medium)
        return label
    }()

    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        return label
    }()

    private lazy var genreLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = .systemFont(ofSize: 13, weight: .medium)
        return label
    }()

    private lazy var favouriteIcon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ic_star")
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didSelectFavIcon))
        gesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(gesture)
        return view
    }()

    private var isFavourited: Bool = false

    var didFavItem: ((_ itemId: Int, _ isFavourited: Bool) -> Void)?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func didSelectFavIcon() {
        isFavourited = !isFavourited
        favouriteIcon.image = isFavourited ? UIImage(named: "ic_star_filled") : UIImage(named: "ic_star")
        didFavItem?(tag, isFavourited)
    }

    private func setupLayout() {
        addSubview(contentStackView)
        contentStackView.anchor(
            top: topAnchor,
            left: leftAnchor,
            bottom: bottomAnchor,
            right: rightAnchor,
            topConstant: 8,
            leftConstant: 12,
            bottomConstant: 8,
            rightConstant: 35
        )

        contentStackView.addArrangedSubview(artworkImage)
        artworkImage.anchor(widthConstant: 60)

        contentStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(releaseYearLabel)
        textStackView.addArrangedSubview(priceLabel)
        textStackView.addArrangedSubview(genreLabel)

        releaseYearLabel.anchor(heightConstant: 20)
        priceLabel.anchor(heightConstant: 20)
        genreLabel.anchor(heightConstant: 20)

        contentStackView.addArrangedSubview(favouriteIcon)
        favouriteIcon.anchor(widthConstant: 20)
    }

    func setup(_ item: PlaylistTrack) {
        tag = item.id
        titleLabel.text = item.name
        genreLabel.text = item.genreName
        priceLabel.text = "\(item.currency) \(item.price)"
        releaseYearLabel.text = item.releaseDate.formatISODate("yyyy")
        isFavourited = item.isFavourite
        favouriteIcon.image = item.isFavourite ? UIImage(named: "ic_star_filled") : UIImage(named: "ic_star")
        artworkImage.loadImage(from: item.artworkUrl100, placeholder: UIImage(named: "ic_placeholder_list"))
    }
}
