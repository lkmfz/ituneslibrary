//
//  PlaylistViewController.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

import UIKit

class PlaylistViewController: UIViewController {

    private lazy var loadingView: LoadingView = {
        let view = LoadingView()
        view.start()
        return view
    }()

    private lazy var emptyStateView: EmptyStateView = {
        let view = EmptyStateView()
        return view
    }()

    private lazy var searchBar: UISearchBar = {
        let view = UISearchBar()
        view.backgroundColor = .gray
        view.autocapitalizationType = .words
        view.returnKeyType = .search
        view.image(for: .search, state: .normal)
        view.placeholder = "Search.."
        view.delegate = self
        return view
    }()

    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.register(cellWithClass: PlaylistTrackCell.self)
        view.dataSource = self
        view.delegate = self
        view.rowHeight = UITableView.automaticDimension
        view.removeTableFooterView()
        return view
    }()

    private var needsToSetupInitialView: Bool = true
    private var lastVisitedDate: Date?
    private var items: [PlaylistTrack] = [] {
        didSet {
            tableView.reloadList()
        }
    }

    let presenter: PlaylistViewToPresenterProtocol

    init(presenter: PlaylistViewToPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdatedBookmark), name: Notification.custom(identifier: "bookmark.updated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLastVisitedDate), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter.fetchItemsInitialView()
    }

    private func configureView() {
        title = "Movies"
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(didTapRefreshButton))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(didTapBookmarkButton))

        view.addSubview(searchBar)
        searchBar.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            left: view.leftAnchor,
            right: view.rightAnchor
        )

        view.addSubview(tableView)
        tableView.anchor(
            top: searchBar.bottomAnchor,
            left: view.leftAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            right: view.rightAnchor
        )

        view.addSubview(emptyStateView)
        emptyStateView.anchor(
            top: searchBar.bottomAnchor,
            left: view.leftAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            right: view.rightAnchor
        )

        view.addSubview(loadingView)
        NSLayoutConstraint.activate([
            self.loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            self.loadingView.heightAnchor.constraint(equalToConstant: 80),
            self.loadingView.widthAnchor.constraint(equalToConstant: 80)
        ])

        tableView.isHidden = true
    }

    private func setInitialStateView(_ state: Bool, errorMessage: String? = nil) {
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
            if self.needsToSetupInitialView, self.items.isEmpty {
                self.needsToSetupInitialView = false
                self.tableView.isHidden = true
                self.emptyStateView.isHidden = false
                self.emptyStateView.show(
                    message: errorMessage ?? "Sorry, result not found..",
                    textColor: .black
                )
            } else {
                self.emptyStateView.isHidden = true
                self.tableView.isHidden = false
            }
        }
    }

    @objc private func onUpdatedBookmark() {
        presenter.refreshList(keyword: searchBar.text)
    }

    @objc private func didTapBookmarkButton() {
        presenter.redirectToBookmarkPage()
    }

    @objc private func didTapRefreshButton() {
        searchBar.text = nil
        presenter.fetchItemsInitialView()
    }

    @objc private func updateLastVisitedDate() {
        lastVisitedDate = Date()
        tableView.reloadData()
    }

    @objc private func performSearch() {
        guard let text = searchBar.text, text.isEmpty else {
            presenter.fetchItemsInitialView()
            return
        }
        presenter.searchItems(keyword: searchBar.text)
    }

    private func setFavoriteMarkItem(_ itemId: Int, _ isFavourited: Bool) {
        guard let item = items.filter({ $0.id == itemId }).first else {
            return
        }
        presenter.setFavourite(item: item, marked: isFavourited)
    }
}

extension PlaylistViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: PlaylistTrackCell.self)
        let item = items[indexPath.row]
        cell.setup(item)
        cell.didFavItem = setFavoriteMarkItem
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        presenter.redirectToDetailsPage(selectedItem: item)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let date = lastVisitedDate?.toString(format: "yyyy/MM/dd, HH:mm:ss") else {
            return nil
        }
        return "Last visited: \(date)"
    }
}

extension PlaylistViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
            return
        }

        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(performSearch), object: nil)
        perform(#selector(performSearch), with: nil, afterDelay: 1.0)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            searchBar.resignFirstResponder()
        }
        performSearch()
    }
}

extension PlaylistViewController: PlaylistPresenterToViewProtocol {

    func showSearchResultItems(_ items: [PlaylistTrack]) {
        self.items = items
        setInitialStateView(needsToSetupInitialView)
    }

    func showError(errorMessage: String?) {
        setInitialStateView(needsToSetupInitialView, errorMessage: errorMessage)
        self.showAlertMessage(title: nil, message: errorMessage)
    }
}
