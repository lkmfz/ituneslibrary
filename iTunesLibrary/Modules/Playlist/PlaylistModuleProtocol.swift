//
//  ModuleProtocol.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 09/07/21.
//

///     [VIEW]
///      ⬆⬇
///  [PRESENTER] ➡ [ROUTER]
///      ⬆⬇
///  [INTERACTOR] ⬅➡ [DATA & API SERVICES]

import UIKit

/// View → Presenter
protocol PlaylistViewToPresenterProtocol: class {
    var view: PlaylistPresenterToViewProtocol? { get set }
    var interactor: PlaylistPresenterToInteractorProtocol? { get set }
    var router: PlaylistPresenterToRouterProtocol? { get set }

    func searchItems(keyword: String?)
    func fetchItemsInitialView()
    func refreshList(keyword: String?)
    func setFavourite(item: PlaylistTrack, marked: Bool)

    func redirectToBookmarkPage()
    func redirectToDetailsPage(selectedItem: PlaylistTrack)
}

/// View ← Presenter
protocol PlaylistPresenterToViewProtocol: class {
    func showSearchResultItems(_ items: [PlaylistTrack])
    func showError(errorMessage: String?)
}

/// Presenter → Interactor
protocol PlaylistPresenterToInteractorProtocol: class {
    var presenter: PlaylistInteractorToPresenterProtocol { get set }
    var apiService: PlaylistNetworkingServiceProtocol { get }
    var persistenceService: PlaylistPersistenceServiceProtocol { get }

    func fetchItemsFromAPI(keyword: String?)
    func fetchItemsFromLocal(keyword: String?, needsAPIDataSync: Bool, initialSetup: Bool)
    func storeItemsToLocal(_ items: [PlaylistTrack])
    func setFavouriteMark(item: PlaylistTrack, marked: Bool)
}

/// Interactor ← Presenter
protocol PlaylistInteractorToPresenterProtocol: class {
    func onReceiveSuccessResultAPI(_  items: [PlaylistTrack])
    func onReceiveResultFromLocal(_  items: [PlaylistTrack], keyword: String?, needsAPIDataSync: Bool, initialSetup: Bool)
    func onFailedResult(_ error: Error)
}

/// Presenter → Router
protocol PlaylistPresenterToRouterProtocol: class {
    static func createModule() -> PlaylistViewController
    func redirectToBookmarkPage(source: PlaylistPresenterToViewProtocol)
    func redirectToDetailsPage(source: PlaylistPresenterToViewProtocol, selectedItem: PlaylistTrack)
}
