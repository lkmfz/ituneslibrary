//
//  BookmarkViewController.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

final class BookmarkViewController: UIViewController {

    private lazy var emptyStateView: EmptyStateView = {
        let view = EmptyStateView()
        return view
    }()

    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.register(cellWithClass: PlaylistTrackCell.self)
        view.dataSource = self
        view.delegate = self
        view.rowHeight = UITableView.automaticDimension
        view.removeTableFooterView()
        return view
    }()

    private var items: [PlaylistTrack] = [] {
        didSet {
            DispatchQueue.main.async {
                self.setContentStateView()
            }
        }
    }

    let presenter: BookmarkViewToPresenterProtocol

    init(presenter: BookmarkViewToPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveFavouriteItems()
    }

    private func configureView() {
        title = "Bookmarks"
        view.backgroundColor = .white

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(didTapCloseButton))

        view.addSubview(tableView)
        tableView.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            left: view.leftAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            right: view.rightAnchor
        )

        view.addSubview(emptyStateView)
        emptyStateView.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            left: view.leftAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            right: view.rightAnchor
        )
    }

    private func setContentStateView() {
        DispatchQueue.main.async {
            if self.items.isEmpty {
                self.tableView.isHidden = true
                self.emptyStateView.isHidden = false
                self.emptyStateView.show(
                    message: "No favourite items",
                    textColor: .black
                )
            } else {
                self.tableView.isHidden = false
                self.emptyStateView.isHidden = true
                self.tableView.reloadData()
            }
        }
    }

    @objc private func didTapCloseButton() {
        dismiss(animated: true, completion: nil)
    }

    private func retrieveFavouriteItems() {
        presenter.retrieveBookmarkItems()
    }

    private func setFavoriteMarkItem(_ itemId: Int, isFavourited marked: Bool) {
        guard let item = items.filter({ $0.id == itemId }).first, !marked else {
            return
        }
        // Store to local storage
        presenter.setFavourite(item: item, marked: marked)

        for (index, item) in items.enumerated() where item.id == itemId {
            // Update tableview cells
            let indexPath = IndexPath(row: index, section: 0)
            tableView.beginUpdates()
            items = items.filter({ $0.id != itemId })
            tableView.deleteRows(at: [indexPath], with: .left)
            tableView.endUpdates()
        }
    }
}

extension BookmarkViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: PlaylistTrackCell.self)
        let item = items[indexPath.row]
        cell.setup(item)
        cell.didFavItem = setFavoriteMarkItem
        return cell
    }
}

extension BookmarkViewController: BookmarkPresenterToViewProtocol {
    func onReceiveResult(_ items: [PlaylistTrack]) {
        self.items = items
    }
}
