//
//  BookmarkRouter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

final class BookmarkRouter: BookmarkPresenterToRouterProtocol {

    /// Create `Bookmark` module
    /// - Returns: Bookmark's view controller
    static func createModule() -> BookmarkViewController {
        typealias BookmarkPresenterProtocol = BookmarkViewToPresenterProtocol & BookmarkInteractorToPresenterProtocol

        let presenter: BookmarkPresenterProtocol = BookmarkPresenter()
        let router: BookmarkPresenterToRouterProtocol = BookmarkRouter()
        let view = BookmarkViewController(presenter: presenter)

        // Service dependecy
        let service = PlaylistPersistenceService(base: PersistenceService())

        let interactor = BookmarkInteractor(presenter: presenter, service: service)

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor

        return view
    }
}
