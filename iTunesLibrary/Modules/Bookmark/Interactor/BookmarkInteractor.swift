//
//  BookmarkInteractor.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

final class BookmarkInteractor: BookmarkPresenterToInteractorProtocol {

    var presenter: BookmarkInteractorToPresenterProtocol
    var service: PlaylistPersistenceServiceProtocol

    init(presenter: BookmarkInteractorToPresenterProtocol, service: PlaylistPersistenceServiceProtocol) {
        self.presenter = presenter
        self.service = service
    }

    /// Retrive favourited items from local storage
    func retrieveBookmarkItems() {
        service.fetchBookmarkedItems { items in
            // Pass the result back to presenter
            self.presenter.onReceiveResultFromLocal(items)
        }
    }

    /// Mark item either as fav/unfav
    /// - Parameters:
    ///   - item: selected item
    ///   - marked: favourited flag
    func setFavourite(item: PlaylistTrack, marked: Bool) {
        service.markFavourite(item: item, marked: marked)
    }
}
