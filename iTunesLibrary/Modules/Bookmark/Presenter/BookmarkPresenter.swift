//
//  BookmarkPresenter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

import UIKit

final class BookmarkPresenter: BookmarkViewToPresenterProtocol {

    var view: BookmarkPresenterToViewProtocol?
    var interactor: BookmarkPresenterToInteractorProtocol?
    var router: BookmarkPresenterToRouterProtocol?

    /// Fetch favourited items from local storage
    func retrieveBookmarkItems() {
        interactor?.retrieveBookmarkItems()
    }

    /// Mark item as either faved/unvafed
    /// - Parameters:
    ///   - item: selected item
    ///   - marked: favourited flag
    func setFavourite(item: PlaylistTrack, marked: Bool) {
        interactor?.setFavourite(item: item, marked: marked)
        // Notify update, for a change in the list
        NotificationCenter.default.post(.init(name: Notification.custom(identifier: "bookmark.updated")))
    }
}

extension BookmarkPresenter: BookmarkInteractorToPresenterProtocol {

    /// On received items from storage
    /// - Parameter items: items list
    func onReceiveResultFromLocal(_ items: [PlaylistTrack]) {
        view?.onReceiveResult(items)
    }
}
