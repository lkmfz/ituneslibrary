//
//  BookmarkModuleProtocol.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 10/07/21.
//

///     [VIEW]
///      ⬆⬇
///  [PRESENTER] ➡ [ROUTER]
///      ⬆⬇
///  [INTERACTOR] ⬅➡ [DATA & API SERVICES]

import UIKit

/// View → Presenter
protocol BookmarkViewToPresenterProtocol: class {
    var view: BookmarkPresenterToViewProtocol? { get set }
    var interactor: BookmarkPresenterToInteractorProtocol? { get set }
    var router: BookmarkPresenterToRouterProtocol? { get set }

    func retrieveBookmarkItems()
    func setFavourite(item: PlaylistTrack, marked: Bool)
}

/// View ← Presenter
protocol BookmarkPresenterToViewProtocol: class {
    func onReceiveResult(_  items: [PlaylistTrack])
}

/// Presenter → Interactor
protocol BookmarkPresenterToInteractorProtocol: class {
    var presenter: BookmarkInteractorToPresenterProtocol { get set }
    var service: PlaylistPersistenceServiceProtocol { get }

    func retrieveBookmarkItems()
    func setFavourite(item: PlaylistTrack, marked: Bool)
}

/// Interactor ← Presenter
protocol BookmarkInteractorToPresenterProtocol: class {
    func onReceiveResultFromLocal(_  items: [PlaylistTrack])
}

/// Presenter → Router
protocol BookmarkPresenterToRouterProtocol: class {
    static func createModule() -> BookmarkViewController
}
