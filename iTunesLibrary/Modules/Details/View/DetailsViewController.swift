//
//  DetailsViewController.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import UIKit

final class DetailsViewController: UIViewController {

    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var artworkImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        return view
    }()

    private lazy var contentStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.spacing = 5
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var trackNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private lazy var genreLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.textColor = .systemGray
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .bold)
        label.textColor = .gray
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private lazy var longDescTextView: UITextView = {
        let view = UITextView()
        view.font = .systemFont(ofSize: 17, weight: .regular)
        view.textColor = .systemGray
        view.isScrollEnabled = false
        view.isEditable = false
        return view
    }()

    private var item: PlaylistTrack? {
        didSet {
            guard let item = item else {
                return
            }
            self.setupContent(item)
        }
    }

    let presenter: DetailsViewToPresenterProtocol

    init(presenter: DetailsViewToPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.retrieveDetailsInformation()
    }

    private func configureView() {
        title = "Details"
        view.backgroundColor = .white
        setupLayout()
    }

    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }

    private func setupLayout() {
        view.addSubview(scrollView)
        scrollView.fillToSuperview()

        scrollView.addSubview(artworkImage)
        artworkImage.anchor(
            top: scrollView.topAnchor,
            left: scrollView.leftAnchor,
            right: scrollView.rightAnchor,
            topConstant: 16,
            widthConstant: view.bounds.width,
            heightConstant: view.bounds.height * 0.3
        )

        scrollView.addSubview(contentStackView)
        contentStackView.anchor(
            top: artworkImage.bottomAnchor,
            left: scrollView.leftAnchor,
            bottom: scrollView.bottomAnchor,
            right: scrollView.rightAnchor,
            topConstant: 16,
            leftConstant: 16,
            bottomConstant: 30,
            rightConstant: 16
        )

        contentStackView.addArrangedSubview(trackNameLabel)
        contentStackView.addArrangedSubview(genreLabel)
        contentStackView.addArrangedSubview(priceLabel)
        contentStackView.addArrangedSubview(longDescTextView)
    }

    private func setupContent(_ item: PlaylistTrack) {
        var title = item.name
        if let releaseYear = item.releaseDate.formatISODate("yyyy") {
            title += " (\(releaseYear))"
        }

        trackNameLabel.text = title
        genreLabel.text = item.genreName
        priceLabel.text = "\(item.currency) \(item.price)"
        longDescTextView.text = item.longDescription
        view.layoutIfNeeded()
    }
}

extension DetailsViewController: DetailsPresenterToViewProtocol {

    func showDetailsInformation(_ item: PlaylistTrack) {
        self.item = item
        presenter.loadArtworkImage(url: item.artworkUrl100)
    }

    func artworkImageLoaded(_ image: UIImage?) {
        artworkImage.image = image
    }
}
