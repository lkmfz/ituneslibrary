//
//  DetailsPresenter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import UIKit

final class DetailsPresenter: DetailsViewToPresenterProtocol {
    var view: DetailsPresenterToViewProtocol?
    var interactor: DetailsPresenterToInteractorProtocol?
    var router: DetailsPresenterToRouterProtocol?

    var trackItem: PlaylistTrack

    init(trackItem: PlaylistTrack) {
        self.trackItem = trackItem
    }

    /// Retrieve details item
    func retrieveDetailsInformation() {
        interactor?.retrieveDetailsInformation(trackItem: trackItem)
    }

    /// Load artwork image
    /// - Parameter url: item's artwork url
    func loadArtworkImage(url: String?) {
        interactor?.loadArtworkImage(url: url, placeholder: nil)
    }
}

extension DetailsPresenter: DetailsInteractorToPresenterProtocol {

    /// On received details item information
    /// - Parameter item: item
    func detailsInformationReceived(_ item: PlaylistTrack) {
        view?.showDetailsInformation(item)
    }

    /// On loaded image
    /// - Parameter image: image
    func artworkImageLoaded(_ image: UIImage?) {
        view?.artworkImageLoaded(image)
    }
}
