//
//  DetailsInteractor.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import UIKit

final class DetailsInteractor: DetailsPresenterToInteractorProtocol {

    var presenter: DetailsInteractorToPresenterProtocol
    var apiService: PlaylistNetworkingServiceProtocol
    var persitenceService: PlaylistPersistenceServiceProtocol

    init(presenter: DetailsInteractorToPresenterProtocol, apiService: PlaylistNetworkingServiceProtocol, persitenceService: PlaylistPersistenceServiceProtocol) {
        self.presenter = presenter
        self.apiService = apiService
        self.persitenceService = persitenceService
    }

    /// Retieve details information
    /// - Parameter trackItem: item object
    func retrieveDetailsInformation(trackItem: PlaylistTrack) {
        presenter.detailsInformationReceived(trackItem)
    }

    /// Load artowrk image
    /// - Parameters:
    ///   - url: image's URL
    ///   - placeholder: placeholder image
    func loadArtworkImage(url: String?, placeholder: UIImage?) {
        apiService.loadMedia(from: url, placeholder: placeholder) { [weak self] image in
            self?.presenter.artworkImageLoaded(image)
        }
    }
}
