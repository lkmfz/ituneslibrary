//
//  DetailsModuleProtocol.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

///     [VIEW]
///      ⬆⬇
///  [PRESENTER] ➡ [ROUTER]
///      ⬆⬇
///  [INTERACTOR] ⬅➡ [DATA & API SERVICES]

import UIKit

/// View → Presenter
protocol DetailsViewToPresenterProtocol: class {
    var view: DetailsPresenterToViewProtocol? { get set }
    var interactor: DetailsPresenterToInteractorProtocol? { get set }
    var router: DetailsPresenterToRouterProtocol? { get set }
    var trackItem: PlaylistTrack { get }

    func retrieveDetailsInformation()
    func loadArtworkImage(url: String?)
}

/// View ← Presenter
protocol DetailsPresenterToViewProtocol: class {
    func showDetailsInformation(_ item: PlaylistTrack)
    func artworkImageLoaded(_ image: UIImage?)
}

/// Presenter → Interactor
protocol DetailsPresenterToInteractorProtocol: class {
    var presenter: DetailsInteractorToPresenterProtocol { get set }
    var apiService: PlaylistNetworkingServiceProtocol { get }
    var persitenceService: PlaylistPersistenceServiceProtocol { get }

    func retrieveDetailsInformation(trackItem: PlaylistTrack)
    func loadArtworkImage(url: String?, placeholder: UIImage?)
}

/// Interactor ← Presenter
protocol DetailsInteractorToPresenterProtocol: class {
    func detailsInformationReceived(_  item: PlaylistTrack)
    func artworkImageLoaded(_  image: UIImage?)
}

/// Presenter → Router
protocol DetailsPresenterToRouterProtocol: class {
    static func createModule(trackItem: PlaylistTrack) -> DetailsViewController
}
