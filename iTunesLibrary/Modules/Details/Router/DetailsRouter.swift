//
//  DetailsRouter.swift
//  iTunesLibrary
//
//  Created by Luqman Fauzi on 11/07/21.
//

import UIKit

final class DetailsRouter: DetailsPresenterToRouterProtocol {

    /// Create `Details` module
    /// - Parameter trackItem: selected item
    /// - Returns: details page view controller
    static func createModule(trackItem: PlaylistTrack) -> DetailsViewController {
        typealias DetailsPresenterProtocol = DetailsViewToPresenterProtocol & DetailsInteractorToPresenterProtocol

        let presenter: DetailsPresenterProtocol = DetailsPresenter(trackItem: trackItem)
        let router: DetailsPresenterToRouterProtocol = DetailsRouter()
        let view = DetailsViewController(presenter: presenter)

        // Service dependencies
        let apiService = PlaylistNetworkingService(base: NetworkingService())
        let persitanceService = PlaylistPersistenceService(base: PersistenceService())

        let interactor = DetailsInteractor(presenter: presenter, apiService: apiService, persitenceService: persitanceService)

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor

        return view
    }
}
